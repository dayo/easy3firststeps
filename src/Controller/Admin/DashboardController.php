<?php

namespace App\Controller\Admin;

use App\Entity\Account;
use App\Entity\Contact;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $accounts = $this->getDoctrine()->getRepository(Account::class)->count([]);
        $accountLast30Days = $this->getAccountsLastDays(30);
        $contacts = $this->getDoctrine()->getRepository(Contact::class)->count([]);

        return $this->render('Admin/dashboard.html.twig', [
            'accounts' => $accounts,
            'contacts' => $contacts,
            'accountLast30Days' => $accountLast30Days,
            'accountLast30DaysSum' => array_sum($accountLast30Days),
        ]);
    }

    private function getAccountsLastDays($days)
    {
        $accountLast30Days = $this->getDoctrine()->getRepository(Account::class)->getAccountsLastDays($days);
        $r = [];
        $begin = new \DateTime('-'.$days.' day');
        $end = new \DateTime();

        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($begin, $interval, $end);

        foreach ($period as $dt) {
            $r[$dt->format("Y-m-d")] = 0;
        }
        foreach ($accountLast30Days as $accountLast30Day) {
            $r[$accountLast30Day->d] = $accountLast30Day->num;
        }

        ksort($r);

        return $r;
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Easy3 Premier pas');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Account', 'fa fa-building', Account::class);
        yield MenuItem::linkToCrud('Contact', 'fa fa-user', Contact::class);
    }
}
